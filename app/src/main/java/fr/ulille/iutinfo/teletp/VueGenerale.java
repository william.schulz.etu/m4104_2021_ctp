package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

public class VueGenerale extends Fragment {

    String salle;
    String poste;
    String DISTANCIEL;
    SuiviViewModel model;
    Spinner spSalle;
    Spinner spPoste;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DISTANCIEL = getActivity().getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;
        model = ((MainActivity)getActivity()).model;

        spSalle = view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> salleAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalle.setAdapter(salleAdapter);

        spPoste = view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> posteAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        spPoste.setAdapter(posteAdapter);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            TextView tvLogin = view.findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        update();

        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                salle = (String)parent.getItemAtPosition(pos);
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                poste = (String)parent.getItemAtPosition(pos);
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        // TODO Q9
    }

    private void update(){
        if(salle.equals(DISTANCIEL)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation(salle);
        }else{
            spPoste.setEnabled(true);
            spPoste.setVisibility(View.VISIBLE);
            model.setLocalisation(salle + poste);
        }

    }
    // TODO Q9
}