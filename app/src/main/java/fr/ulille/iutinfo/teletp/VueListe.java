package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class VueListe extends Fragment implements Observer<Integer> {

    SuiviViewModel model;
    
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_liste, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnToGenerale).setOnClickListener(view1 -> NavHostFragment.findNavController(VueListe.this)
                .navigate(R.id.liste_to_generale));

        model = ((MainActivity)getActivity()).model;
        RecyclerView rvQuestions = getActivity().findViewById(R.id.rvQuestions);
        rvQuestions.setLayoutManager(new LinearLayoutManager(getActivity()));
        SuiviAdapter adapter = new SuiviAdapter(model);
        rvQuestions.setAdapter(adapter);
        update();

        // TODO Q8
    }

    private void update(){

    }

    @Override
    public void onChanged(Integer integer) {
        update();
    }
}