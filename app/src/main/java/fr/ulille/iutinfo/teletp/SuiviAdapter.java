package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    final SuiviViewModel model;

    public SuiviAdapter(SuiviViewModel model){
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuiviAdapter.ViewHolder holder, int position) {
        String question = model.getQuestions(position);
        holder.setQuestion(question);
    }

    @Override
    public int getItemCount() {
        return model.getQuestionLength();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private final View itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        public void setQuestion(String question){
            ((TextView) itemView.findViewById(R.id.question)).setText(question);
        }
    }
    // TODO Q7
}
